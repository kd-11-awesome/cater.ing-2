from django.shortcuts import render
from django.shortcuts import redirect
from .models import FormPemesanan

def order(request):

    if request.method == 'POST':

        nama_pemesan = request.POST.get('nama')
        pilihan_menu = request.POST.get('menu')
        alamat = request.POST.get('alamat')
        pembayaran = request.POST.get('pembayaran')
        no_telp = request.POST.get('telepon')
        notes = request.POST.get('notes')

        form_pemesanan = FormPemesanan(namaPemesan = nama_pemesan, pilihanMenu = pilihan_menu, alamatPengiriman = alamat, jenisPembayaran = pembayaran, nomorTelepon = no_telp, notes = notes)
        form_pemesanan.save()

        return render(request, 'Form Pemesanan.html')

    else:
        return render(request, 'Form Pemesanan.html')
