from django.db import models

# Create your models here.

class FormPemesanan(models.Model):
	namaPemesan = models.CharField(max_length = 50, null="False", blank="False")
	pilihanMenu = models.CharField(max_length = 500, null="False", blank="False")
	alamatPengiriman = models.CharField(max_length = 80, null="False", blank="False")
	jenisPembayaran = models.CharField(max_length = 30, null="False", blank="False")
	nomorTelepon = models.IntegerField(default=0, null="False", blank="False")
	notes = models.TextField()

	def __str__(self):
		return self.namaPemesan
