from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.urls import reverse
from .views import order
from .models import FormPemesanan
from .apps import OrderConfig

# Create your tests here.

class UrlUnitTest(TestCase):

    # def test_form_url_is_exist(self):
    # 	response = Client().get('/order/')
    # 	self.assertEqual(response.status_code, 200)

    def test_login_exist(self):
        response = self.client.get(reverse('order:order'))
        self.assertEqual(response.status_code, 200)

    def test_form_using_order_func(self):
        found = resolve('/order/')
        self.assertEqual(found.func, order)
    
    # def test_form_using_index_func(self):
    # 	found = resolve('/order/')
    # 	self.assertEqual(found.func, order)

class ViewsUnitTest(TestCase):

    def setUp(self):
        self.client = Client()
        self.order = reverse('order:order')
    
    # def test_form_using_index_template(self):
    #     response = Client().get('/order/')
    #     self.assertTemplateUsed(response, 'Form Pemesanan.html')

    def test_form_order_GET(self):
        response = self.client.get(self.order)

        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'Form Pemesanan.html')


    def test_form_order_POST(self):
        response = self.client.post(reverse('order:order'), 
            data = {'namaPemesan':'dito', 'pilihanMenu':'ayam', 'alamatPengiriman':'jaksel', 
            'jenisPembayaran':'transfer', 'nomorTelepon':1234567, 'notes':'pedas'})
        self.assertEquals(response.status_code, 200)
    # def test_form_can_be_used(self):
    # 	isi = Client().post('/order/', status = {'status' : 'Saya senang'})
    # 	self.assertFalse(Status.objects.filter(status='Saya senang'))
    
    # def test_form_validation_for_blank_items(self):
    # 	form = FormStatus(data={'status': ''})
    # 	self.assertFalse(form.is_valid())
    # 	self.assertEqual(
    # 	    form.errors['status'],
    # 	    ["This field is required."]
    # 	)

class ModelsUnitTest(TestCase):

    def setUp(self):
        new_namaPemesan = FormPemesanan.objects.create(namaPemesan = '', pilihanMenu = '', alamatPengiriman = '', 
            jenisPembayaran = '', nomorTelepon = 0, notes = '')

    def test_model_can_create_new_order(self):
        self.assertEqual(FormPemesanan.objects.all().count(), 1)