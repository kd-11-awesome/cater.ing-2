from django.db import models

# Create your models here.
class Feedback(models.Model):
    menu = models.CharField(max_length=50)
    message = models.TextField(blank = True)

    def __str__(self):
        return self.menu
