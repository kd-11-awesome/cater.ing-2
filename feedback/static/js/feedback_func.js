$(document).ready(function() {

    load_json_data('menu_type');

    function load_json_data(id) {
        var html_code = '';
        $.getJSON('menu_type/', function(data){
            html_code += '<option value="">Select Menu </option>';
            $.each(data, function(key, value){
                if(id=='menu_type') {
                    if(value.parent_id == "0") {
                        html_code += '<option value="'+value.id+'">'+value.name+'</option>';
                    }
                }
            });
            $('#'+id).html(html_code);
        })
    }

});