from django.urls import path
from feedback import views

app_name = 'feedback'

urlpatterns = [
    path('', views.feedback, name='feedback'),
    path('menu_type/', views.menu_type, name='menu_type'),

]
