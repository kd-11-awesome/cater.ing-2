from django.shortcuts import render
from feedback.models import Feedback

# Create your views here.
def feedback(request):
    if request.method == 'POST':
        selected_menu = request.POST.get('menu_type')
        if selected_menu == 1:
            menu = "Salad"
        elif selected_menu == 2:
            menu = "Steak"
        else:
            menu = "Halibut"
        message = request.POST.get('feedback')

        feedback = Feedback(menu=menu, message=message)
        feedback.save()
        return render(request, 'feedback.html', {'answer' : 'Thank you for submitting'})
    else:
        return render(request, 'feedback.html')

def menu_type(request):
    return render(request, 'data.json')

