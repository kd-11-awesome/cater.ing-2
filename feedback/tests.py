from django.contrib.auth.models import User
from django.urls import resolve, reverse
from django.test import TestCase
from feedback.views import feedback
from feedback.models import Feedback

# Create your tests here.
class Feedback_Unit_Test(TestCase):
    def test_feedback_exist(self):
        response = self.client.get(reverse('feedback:feedback'))
        self.assertEqual(response.status_code, 200)

    def test_feedback_using_feedback_func(self):
        found = resolve('/feedback/')
        self.assertEqual(found.func, feedback)

    def test_feedback_using_feedback_template(self):
        response = self.client.get(reverse('feedback:feedback'))
        self.assertTemplateUsed(response, 'feedback.html')

        self.assertContains(response, 'Tell us about your experience while ordering in catering')

    def test_model_can_create_new_feedback(self):
        new_feedback = Feedback.objects.create(menu='Salad', message='Sumpah enak banget parah')
        self.assertEqual(Feedback.objects.all().count(),1)
