from django.test import TestCase

# Create your tests here.
from django.test import SimpleTestCase
from django.urls import resolve, reverse
from menu.views import *

# Create your tests here.
class Menu_Unit_Test(TestCase):
    def test_menupage_is_exist(self):
        response = self.client.get(reverse('menu:menu'))
        self.assertEqual(response.status_code, 200)

    def test_menupage_is_exist(self):
        url = resolve('/menu/')
        self.assertEqual(url.func, menu)
