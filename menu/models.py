from django.db import models

# Create your models here.
class RequestMenu(models.Model):
    namaMakanan = models.CharField(max_length=50)
    kategoriMakanan = models.CharField(max_length=30)
    deskripsiMakanan = models.TextField()
