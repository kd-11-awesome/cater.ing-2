from django.http import HttpResponseRedirect
from django.shortcuts import render
from . import models, forms


# Create your views here.
def menu(request):
    return render(request, "menupage.html")

def requestMenu(request):
    menu_baru = forms.RequestMenu()

    if request.method == "POST":
        models.RequestMenu.objects.create(
            namaMakanan = request.POST['namaMakanan'],
            kategoriMakanan = request.POST['kategoriMakanan'],
            deskripsiMakanan = request.POST['deskripsiMakanan'],
        )

        return HttpResponseRedirect("/menu/")

    context = {
        'menu_baru':menu_baru
    }

    return render(request, "request.html", context)
