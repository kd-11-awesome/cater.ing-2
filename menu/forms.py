from django import forms

class RequestMenu(forms.Form):
    namaMakanan = forms.CharField(
        label="Nama Makanan"
        )
    kategoriMakanan = forms.CharField(
        label="Kategori Makanan"
        )
    deskripsiMakanan = forms.CharField(
        widget=forms.TextInput(),
        label="Deskripsi Makanan"
        )
