from django.test import TestCase
from django.test import SimpleTestCase
from django.urls import resolve, reverse
from homepage.views import homepage, qna, about_us, our_team
from homepage.models import Answer


# Create your tests here.
class Homepage_Unit_Test(TestCase):
    def test_homepage_is_exist(self):
        response = self.client.get(reverse('homepage:homepage'))
        self.assertEqual(response.status_code, 200)
    
    def test_qna_page_page_is_exist(self):
        response = self.client.get(reverse('homepage:qna'))
        self.assertEqual(response.status_code, 200)
    
    
    def test_about_us_page_is_exist(self):
        response = self.client.get(reverse('homepage:about-us'))
        self.assertEqual(response.status_code, 200)
    
    def test_our_team_page_is_exist(self):
        response = self.client.get(reverse('homepage:our-team'))
        self.assertEqual(response.status_code, 200)

    def test_homepage_is_exist(self):
        url = resolve('/')
        self.assertEqual(url.func, homepage)

    def test_qna_page_is_exist(self):
        url = resolve('/qna/')
        self.assertEqual(url.func, qna)
    
    def test_qna_page_is_exist(self):
        url = resolve('/about-us/')
        self.assertEqual(url.func, about_us)

    def test_qna_page_is_exist(self):
        url = resolve('/our-team/')
        self.assertEqual(url.func, our_team)

    def test_model_can_create_new_answer(self):
        new_answer = Answer.objects.create(key='pesan', answer='bisa hubungi kami di +62-812-9128-0150')
        self.assertEqual(Answer.objects.all().count(),1)
    
    def test_qna_GET(self):
        response = self.client.get(reverse('homepage:qna'))

        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'QnA-page.html')

    def test_qna_POST(self):
        Answer.objects.create(
            key='pesan',
            answer='bisa hubungi kami di +62-812-9128-0150'
            )
        Answer.objects.create(
            key='notfound',
            answer=':(('
            )
        response = self.client.post(reverse('homepage:qna'), data={'keyword':'pesan'})

        self.assertEquals(response.status_code, 200)
