from django.shortcuts import render

# Create your views here.

from django.shortcuts import redirect
from django.contrib.auth import authenticate, login as auth_login, logout as auth_logout

# Create your views here.

def login(request):
    if request.user.is_authenticated:
        return redirect('homepage:homepage')
    else:
        if request.method == "POST":
            username = request.POST.get('username')
            password = request.POST.get('password')
            user = authenticate(
                request, username=username, password=password
            )
            if user is not None:
                auth_login(request, user)
                request.session['username'] = user.first_name
                return redirect('homepage:homepage')
            else:
                return render(
                    request, 'login.html', {
                        'error' : 'invalid credential'
                    }
                )

    return render(request, 'login.html')

def logout(request):
    auth_logout(request)
    return redirect('homepage:homepage')

