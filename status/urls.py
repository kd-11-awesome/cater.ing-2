from django.urls import path
from . import views

app_name = 'status'

urlpatterns = [
    path('', views.article, name='article'),
    path('satu/', views.status, name='status'),
    path('dua/', views.article_dua, name='article_dua'),
]
