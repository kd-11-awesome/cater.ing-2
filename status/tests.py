from django.test import TestCase, Client, LiveServerTestCase
import datetime
from django.urls import resolve, reverse
from status import views
from .views import status
from .models import create_status

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time

class StatusTestCase(TestCase):
    def test_about_url_exists(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)

    def test_about_using_about_func(self):
        found = resolve(reverse('status:status'))
        self.assertEqual(found.func, views.status)

    def test_model_can_add_new_status(self):
        new_status = create_status.objects.create(statusfield='ya allah',time=datetime.datetime.now())
        counting_all_status = create_status.objects.all().count()
        self.assertEqual(counting_all_status,1)

